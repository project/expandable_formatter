INTRODUCTION
------------

The Expandable Formatter module provides a field formatter that expands
and collapses to limit the amount of text that shows on the page. This
helps maintain a desired page layout regardless of how much content is
entered in the field.

 * For a full description of the module, please visit the project page:
   https://drupal.org/project/expandable_formatter

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/expandable_formatter


REQUIREMENTS
------------

None.


INSTALLATION
------------

Enable the module.


CONFIGURATION
-------------

Manage the display for a field and choose the "Expandable" option. There
are multiple settings available that provide a little flexibility to how
it operates.
